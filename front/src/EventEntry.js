import React from 'react';
import { withRouter } from 'react-router';
import { Form, Input, Button, Typography } from 'antd';

const { Title } = Typography;
const { TextArea } = Input;

class EventEntry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          eventName: '',
          description: '',
          candidates: ''
            // ここにstateの初期値を書きます
        };
    }

    registerEvent = (evt) => {
        evt.preventDefault();
        // ここにサーバーにデータを送信する処理を書きます。
        fetch("/api/events", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then(res => res.json()).then(json => {
            this.props.history.push(`/event/${json.eventId}`);
        });
    }

    onChangeEventName = (evt) => {
        // 入力された値(evt.target.value)をsetStateします
        this.setState({
            eventName: evt.target.value
        });
    }

    onChangeDescription = (evt) => {
      this.setState({
          description: evt.target.value
      });
    }

    onChangeCandidates = (evt) => {
      this.setState({
          candidates: evt.target.value
      });
    }

    // <div>内に入力フォームを作ります
    render() {
        return (
            <div>
                <Title level={2}>イベント登録</Title>
                <Form onSubmit={this.registerEvent}>
                  <Form.Item label="イベント名">
                    <Input value={this.state.eventName} onChange={this.onChangeEventName} placeholder="送別会" />
                  </Form.Item>
                  <Form.Item label="説明">
                    <TextArea value={this.state.description} onChange={this.onChangeDescription} placeholder="送別会の日程調整しましょう！出欠〆切は◯日。" />
                  </Form.Item>
                  <Form.Item label="候補日程">
                    <TextArea value={this.state.candidates} onChange={this.onChangeCandidates} placeholder="8/7(月) 20:00～&#13;&#10;8/8(火) 20:00～&#13;&#10;8/9(水) 21:00～" />
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary" htmlType="submit">出欠表を作る</Button>
                  </Form.Item>
                </Form>
            </div>
        );
    }

}

export default withRouter(EventEntry);
